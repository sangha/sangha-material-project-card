# \<sangha-material-project-card\>

Project overview with more link and add to cart button.

```html
<sangha-material-project-card
  project='[{"logo":"url", "name":"the_name", "summary": "short text", "license": "MIT", "website": "https://project.com", "repository": "https://gitlab.com/sangha"}]'
  baseUrl="https://sangha_domain.com/projects/"
  cartAvailable="false (whether to show add to cart)"
></sangha-material-project-card>
```

## Development

```bash
# Get dependencies
$ npm install

# Demo site
$ npm start

# Run tests
$ npm test
```
